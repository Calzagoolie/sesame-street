from flask import Flask, request, g
from flask_restful import abort, Resource, Api
from peewee import *
from playhouse.shortcuts import model_to_dict

app = Flask("Sesame Street")
api = Api(app)

db = SqliteDatabase("sesamestreet.db")

class BaseModel(Model):

    class Meta():
        database = db


class Character(BaseModel):
    name = CharField()
    colour = CharField()


db.connect()

db.create_tables([Character])


class API_Characters(Resource):
    def get(self):
        characters = Character.select()
        return [model_to_dict(c) for c in characters]


class API_Character(Resource):
    def get(self, character_id):
        try:
            return model_to_dict(Character.get_by_id(character_id))
        except:
            return "Character not found"
